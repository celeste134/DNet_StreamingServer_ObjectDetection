import numpy as np
import os
import sys
import tensorflow as tf
import cv2
sys.path.append('..')
import label_map_util
import time

class OBJECT_DETECTION:
    
    MODEL_DICT = {
        'ssd_mobilenet_v1':'ssd_mobilenet_v1_coco_2018_01_28',
        'ssd_mobilenet_v1_quentized':'ssd_mobilenet_v1_0.75_depth_quantized_300x300_coco14_sync_2018_07_18',
        'ssd_modilenet_v1_ppn_300x300':'ssd_mobilenet_v1_ppn_shared_box_predictor_300x300_coco14_sync_2018_07_03',
        'ssd_resnet50_v1':'ssd_resnet50_v1_fpn_shared_box_predictor_640x640_coco14_sync_2018_07_03',
        'ssd_mobilenet_v2':'ssd_mobilenet_v2_coco_2018_03_29',
        'ssdlite_mobilenet_v2':'ssdlite_mobilenet_v2_coco_2018_05_09',
        'ssd_inception_v2':'ssd_inception_v2_coco_2018_01_28',
        'faster_rcnn_inception_v2':'faster_rcnn_inception_v2_coco_2018_01_28',
        'rfcn_resnet101':'rfcn_resnet101_coco_2018_01_28',
        'mask_rcnn_inception_v2':'mask_rcnn_inception_v2_coco_2018_01_28'
    }
    
    
    
    NUM_CLASSES = 90

    #PATH_TO_LABELS = os.path.join('/root/dnet/data', 'mscoco_label_map.pbtxt')
    PATH_TO_LABELS = 'mscoco_label_map.pbtxt'
    LABEL_MAP = label_map_util.load_labelmap(PATH_TO_LABELS)
    CATEGORIES = label_map_util.convert_label_map_to_categories(LABEL_MAP, max_num_classes=NUM_CLASSES)
    CATEGORY_INDEX = label_map_util.create_category_index(CATEGORIES)
    
    input_image_list = []
    
    
    def __init__(self, model_type, fraction):
        
        #MODEL_PATH = 'pb/' + self.MODEL_DICT[model_type] + '.pb'
        MODEL_PATH = 'pb/' + self.MODEL_DICT[model_type] + '.pb'
        
        detection_graph = tf.Graph()
        
        with detection_graph.as_default():
            graph_def = tf.GraphDef()
            with tf.gfile.GFile(MODEL_PATH, 'rb') as fid:
                serialized_graph = fid.read()
                graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(graph_def, name='')
        
        print ("Model Initialized") 
        
        self.image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
        self.detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
        self.detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
        self.detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
        self.num_detections = detection_graph.get_tensor_by_name('num_detections:0')
                   
        gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=fraction)
        self.sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options), graph=detection_graph)
        
        print ("Session Created")
        
        
    def append_to_list(self, nparr):

        nparr = nparr.ravel().reshape(720,1280,3)
        self.input_image_list.append(nparr)
        
        
        
        
    def inference(self):
        input_image_tensor = np.asarray(self.input_image_list)
        print ('------')
        
        s_time = time.time()
        
        (boxes, scores, classes) = self.sess.run(
                [self.detection_boxes, self.detection_scores, self.detection_classes],
                feed_dict={self.image_tensor: input_image_tensor})
        
        print ('{} images : {}'.format(len(self.input_image_list), time.time()-s_time))
        
        return self.generate_result(boxes, scores, classes)
           
            
            
    def generate_result(self, boxes, scores, classes):
        # each frame : len(boxes) 
        total_results = []

        for frame_num in range(len(scores)):
            ## each frame
            org_img = self.input_image_list[frame_num]
            #org_img = cv2.cvtColor(org_img, cv2.COLOR_BGR2RGB)

            total_results.append([])

            each_frame_scores = []

            for detect_num in range(len(scores[frame_num])):
                ## detection count 
                if scores[frame_num][detect_num] > 0.5:
                    each_frame_scores.append(scores[frame_num][detect_num])
                else:
                    break

            for detection in range(len(each_frame_scores)):

                total_results[frame_num].append({})
                total_results[frame_num][detection]['type'] = self.CATEGORY_INDEX[classes[frame_num][detection]]['name']
                ymin, xmin, ymax, xmax = boxes[frame_num][detection]
                left, right, top, bottom = (xmin * 1280, xmax*1280, ymin*720, ymax*720)

                x = int(left)
                y = int(top)
                w = int(right - left)
                h = int(bottom - top)

                cropped_img = org_img[y:y+h, x:x+w]                
                r, buf = cv2.imencode('.jpg', cropped_img)
                
                total_results[frame_num][detection]['x'] = x
                total_results[frame_num][detection]['y'] = y
                total_results[frame_num][detection]['w'] = w
                total_results[frame_num][detection]['h'] = h
                total_results[frame_num][detection]['prob'] = each_frame_scores[detection]
                total_results[frame_num][detection]['img'] = buf
                
        self.input_image_list = []

        return total_results



        
